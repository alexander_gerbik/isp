import collections
import re

Token = collections.namedtuple('Token', 'type value')


def tokenize(code):
    token_specification = [
        ('FLOAT', r'[-+]?([0-9]+)?\.(?(2)[0-9]*|[0-9]+)([eE][-+]?[0-9]+)?'),
        ('INT', r'\d+'),
        ('STRING', r'"(?:[^"\\]|\\.)*"'),
        ('TRUE', r'true'),
        ('FALSE', r'false'),
        ('NULL', r'null'),
        ('OPEN_BRACES', r'{'),
        ('CLOSING_BRACES', r'}'),
        ('ITEM_SEPARATOR', r','),
        ('KEY_VALUE_SEPARATOR', r':'),
        ('OPEN_BRACKETS', r'\['),
        ('CLOSING_BRACKETS', r'\]'),
        ('WHITESPACE', r'\s+'),
        ('MISMATCH', r'.+'),
    ]
    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
    for mo in re.finditer(tok_regex, code):
        kind = mo.lastgroup
        value = mo.group(kind)
        if kind == 'WHITESPACE':
            continue
        if kind == 'MISMATCH':
            raise RuntimeError(f'Unexpected token: {value!r}')
        if kind == 'INT':
            value = int(value)
        elif kind == 'FLOAT':
            value = float(value)
        elif kind == 'STRING':
            value = value[1:-1]
            value = value.replace('\\"', '"')
        elif kind == 'TRUE':
            value = True
        elif kind == 'FALSE':
            value = False
        elif kind == 'NULL':
            value = None
        yield Token(kind, value)


class TokenIterator(object):
    def __init__(self, tokens):
        self._tokens = tokens
        self._next = None

    def __next__(self):
        if self._next is None:
            return next(self._tokens)
        t, self._next = self._next, None
        return t

    def __iter__(self):
        return self

    def assert_next(self, token_type):
        n = next(self)
        if token_type != n.type:
            raise SyntaxError('Expected token of type {}, got token of type {}'
                              ' instead'.format(token_type, n.type))

    @property
    def next(self):
        if self._next is None:
            self._next = next(self._tokens)
        return self._next
