from functools import wraps

_cache = {}


def cached(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        frozen_dict = tuple(sorted(kwargs.items()))
        entry = (function, args, frozen_dict)
        if entry in _cache:
            return _cache[entry]
        result = function(*args, **kwargs)
        _cache[entry] = result
        return result

    return wrapper
