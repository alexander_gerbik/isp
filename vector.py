from numbers import Complex
from collections.abc import Iterable


class Vector(object):
    def __init__(self, iterable):
        if not isinstance(iterable, Iterable):
            raise TypeError(
                '{} constructor argument must support iteration'.format(
                    self.__class__.__name__))
        self._values = list(iterable)
        if not all(isinstance(v, Complex) for v in self._values):
            raise TypeError(
                '{} constructor argument must be iterable of numbers'.format(
                    self.__class__.__name__))
        if not self._values:
            raise ValueError('iterable should not be empty')

    @classmethod
    def zeroes(cls, n):
        return cls(0.0 for _ in range(n))

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return iter(self._values)

    @staticmethod
    def _ensure_equal_length(a, b):
        if len(a) != len(b):
            raise ValueError('Vectors length should be equal')

    def __add__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        Vector._ensure_equal_length(self, other)
        return Vector(i + j for i, j in zip(self, other))

    def __sub__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        Vector._ensure_equal_length(self, other)
        return Vector(i - j for i, j in zip(self, other))

    def __eq__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        return len(self) == len(other) and all(
            i == j for i, j in zip(self, other))

    def dot(self, other):
        Vector._ensure_equal_length(self, other)
        return sum(i * j for i, j in zip(self, other))

    def __mul__(self, other):
        if isinstance(other, Vector):
            return self.dot(other)
        if isinstance(other, Complex):
            return Vector(other * i for i in self)
        return NotImplemented

    def __rmul__(self, other):
        return self.__mul__(other)

    def __getitem__(self, item):
        return self._values[item]

    def __str__(self):
        return str(self._values)

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, self._values)
