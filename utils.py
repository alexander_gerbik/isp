from itertools import tee


def nwise(iterable, n):
    its = tee(iterable, n)
    for i in range(n):
        for _ in range(i):
            next(its[i], None)
    return zip(*its)
