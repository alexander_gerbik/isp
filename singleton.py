class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        original_new = cls.__new__

        def singleton_new(cls, *args, **kwargs):
            if cls.instance is None:
                cls.instance = original_new(cls, *args, **kwargs)
            return cls.instance

        cls.instance = None
        cls.__new__ = staticmethod(singleton_new)
