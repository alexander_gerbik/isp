from collections import OrderedDict
from inspect import Signature, Parameter


class Field(object):
    def _get_storage_name(self):
        return '_' + self._name

    def __get__(self, instance, owner):
        if instance is not None:
            return getattr(instance, self._storage_name)
        return self

    def __set__(self, instance, value):
        setattr(instance, self._storage_name, value)

    def __delete__(self, instance):
        delattr(instance, self._storage_name)

    def __set_name__(self, owner, name):
        self._owner = owner
        self._name = name
        self._storage_name = self._get_storage_name()


class ValidatingField(Field):
    def validate(self, value):
        pass

    def __set__(self, instance, value):
        self.validate(value)
        super().__set__(instance, value)


class TypeValidatingField(ValidatingField):
    type = None

    def validate(self, value):
        if not isinstance(value, self.type):
            raise TypeError(f'value is not an instance of {self.type}')
        super().validate(value)


class StringField(TypeValidatingField):
    type = str


class IntField(TypeValidatingField):
    type = int


class FloatField(TypeValidatingField):
    type = float


class PositiveField(ValidatingField):
    def validate(self, value):
        if value < 0:
            raise ValueError('Value should not be less than zero')
        super().validate(value)


class PositiveIntField(IntField, PositiveField):
    pass


class PositiveFloatField(FloatField, PositiveField):
    pass


class Model(type):
    @classmethod
    def __prepare__(mcs, name, bases):
        return OrderedDict()

    def __new__(mcs, name, bases, namespace, **kwargs):
        fields = tuple(k for k, v in namespace.items() if isinstance(v, Field))
        bases = bases + (_Model,)
        namespace['_fields'] = fields
        cls = super().__new__(mcs, name, bases, dict(namespace))
        fields = []
        for c in reversed(cls.__mro__):
            if hasattr(c, '_fields'):
                fields.extend(c._fields)
        cls._signature = _make_signature(fields)
        return cls


def _make_signature(names):
    return Signature(
        Parameter(name, Parameter.POSITIONAL_OR_KEYWORD) for name in names)


class _Model(object):
    def __init__(self, *args, **kwargs):
        bound = self.__class__._signature.bind(*args, **kwargs)
        for field, value in bound.arguments.items():
            setattr(self, field, value)
