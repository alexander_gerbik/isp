from collections.abc import Sequence, Mapping
from my_tokenize import tokenize, TokenIterator


class UnformattableTypeError(TypeError):
    pass


def to_json(o):
    if o is None:
        return 'null'
    if isinstance(o, bool):
        return 'true' if o else 'false'
    if isinstance(o, (int, float)):
        return str(o)
    if isinstance(o, str):
        o = o.replace('"', '\\"')
        return '"{}"'.format(o)
    if isinstance(o, Mapping):
        return '{{{}}}'.format(
            ', '.join('{}: {}'.format(
                to_json(str(k)), to_json(v)) for k, v in o.items()))
    if isinstance(o, Sequence):
        return '[{}]'.format(', '.join(to_json(i) for i in o))
    raise UnformattableTypeError(type(o))


def parse_json(source):
    token_iterator = TokenIterator(tokenize(source))
    result = _value(token_iterator)
    left = list(token_iterator)
    if left:
        raise SyntaxError(
            'Unexpected tokens at the end of a string: {}'.format(left))
    return result


def _value(token_iterator):
    token = token_iterator.next
    if token.type in {'STRING', 'INT', 'FLOAT', 'TRUE', 'FALSE', 'NULL'}:
        next(token_iterator)
        return token.value
    if token.type == 'OPEN_BRACES':
        return _object(token_iterator)
    if token.type == 'OPEN_BRACKETS':
        return _array(token_iterator)
    raise SyntaxError('Unexpected token: {}'.format(token))


def _array(token_iterator):
    result = []
    token_iterator.assert_next('OPEN_BRACKETS')
    if token_iterator.next.type == 'CLOSING_BRACKETS':
        next(token_iterator)
        return result
    result.append(_value(token_iterator))
    while True:
        if token_iterator.next.type == 'CLOSING_BRACKETS':
            break
        token_iterator.assert_next('ITEM_SEPARATOR')
        result.append(_value(token_iterator))
    next(token_iterator)
    return result


def _object(token_iterator):
    result = {}
    token_iterator.assert_next('OPEN_BRACES')
    if token_iterator.next.type == 'CLOSING_BRACES':
        next(token_iterator)
        return result
    k, v = _pair(token_iterator)
    result[k] = v
    while True:
        if token_iterator.next.type == 'CLOSING_BRACES':
            break
        token_iterator.assert_next('ITEM_SEPARATOR')
        k, v = _pair(token_iterator)
        result[k] = v
    next(token_iterator)
    return result


def _pair(token_iterator):
    k = token_iterator.next.value
    token_iterator.assert_next('STRING')
    token_iterator.assert_next('KEY_VALUE_SEPARATOR')
    v = _value(token_iterator)
    return k, v
