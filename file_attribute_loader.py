class FileAttributeLoader(type):
    def __new__(mcs, name, bases, namespace, **kwargs):
        file = kwargs.pop('file', None)
        if file is None:
            raise RuntimeError('Expected keyword argument "file"'
                               ' in class definition')
        attrs = _read_atts(file)
        _ensure_valid_identifiers(attrs)
        namespace.update(attrs)
        return super().__new__(mcs, name, bases, namespace)


def _ensure_valid_identifiers(namespace):
    for key in namespace:
        if not key.isidentifier():
            raise SyntaxError(f'"{key}" is not a valid python identifier')


def _read_atts(file):
    lines = (line.strip() for line in file)
    return dict(line.split('=') for line in lines if line)
