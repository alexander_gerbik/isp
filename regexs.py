import re

_email_re = re.compile(r"""
    ^[a-zA-Z0-9_.+-]+  # username
    @[a-zA-Z0-9-]+  # host
    \.[a-zA-Z0-9-.]+$  # domains
""", re.X)

_float_re = re.compile(r"""
    ^[-+]?([0-9]+)?\.  # integral part
    (?(1)[0-9]*|[0-9]+)  # fractional part
    ([eE][-+]?[0-9]+)?$  # exponent
""", re.X)

_uri_re = re.compile(r"""
    (?:(?P<scheme>[^:]*)://)#?
    (?P<userinfo>
        (?P<username>[^:@]+)
        (?::(?P<password>[^@]+))?
    @)?
    (?P<host>[^/:]+)
    (?::(?P<port>[0-9]+))?
    (?P<path>[^?#]*)?
    (?:\?(?P<query>[^#]*))?
    (?:\#(?P<fragment>.*))?
""", re.X)


def is_email(s):
    return _email_re.fullmatch(s) is not None


def is_float(s):
    return _float_re.fullmatch(s) is not None


def parse_uri(s):
    match = _uri_re.fullmatch(s)
    if match is None:
        raise ValueError('Given string is not an uri')
    return match.groupdict()
