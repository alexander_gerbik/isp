from collections import defaultdict


class ExpandingDict(defaultdict):
    def __init__(self, **kwargs):
        super(ExpandingDict, self).__init__(ExpandingDict, **kwargs)

    def __repr__(self):
        return '{}(**{{{}}})'.format(
            self.__class__.__name__,
            ', '.join(f'{k!r}: {v!r}' for k, v in self.items())
        )

    def __str__(self):
        def s(o):
            return str(o) if isinstance(o, ExpandingDict) else repr(o)

        return '{{{}}}'.format(
            ', '.join(f'{s(k)}: {s(v)}' for k, v in self.items()))
