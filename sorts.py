from itertools import chain

from utils import nwise


def _merge(left_array, right_array):
    ll, rl = len(left_array), len(right_array)
    result = []
    li, ri = 0, 0
    for i in range(ll + rl):
        if li != ll and (ri == rl or left_array[li] <= right_array[ri]):
            result.append(left_array[li])
            li += 1
        else:
            result.append(right_array[ri])
            ri += 1
    return result


def merge_sort(lst):
    n = len(lst)
    if n == 1:
        return lst
    k = n // 2
    return _merge(merge_sort(lst[:k]), merge_sort(lst[k:]))


def is_sorted(iterable):
    return all(i <= j for i, j in nwise(iterable, 2))


def _partition(lst, l, r):
    pivot = lst[r]
    i = l
    for j in range(l, r):
        if lst[j] < pivot:
            lst[i], lst[j] = lst[j], lst[i]
            i += 1
    lst[r], lst[i] = lst[i], lst[r]
    return i


def quicksort(lst):
    lst = lst.copy()
    stack = [(0, len(lst) - 1)]
    while stack:
        li, ri = stack.pop()
        if li >= ri:
            continue
        p = _partition(lst, li, ri)
        a, b = (li, p - 1), (p + 1, ri)
        if b[1] - b[0] > a[1] - a[0]:
            a, b = b, a
        stack.append(a)
        stack.append(b)
    return lst


def radix_sort(lst):
    lst = [(i, i, 0) for i in lst]
    while any(el[1] for el in lst):
        lst = [(i, *divmod(j, 2)) for i, j, _ in lst]
        lst = list(chain(filter(lambda el: not el[2], lst),
                         filter(lambda el: el[2], lst)))
    lst = [i for i, _, _ in lst]
    return lst


def wrapper(s, sort):
    values = list(map(int, s.split()))
    return sort(values)
