from itertools import islice

from fib import fibonacci
from utils import nwise


def test_thousand_values():
    n = 1000
    assert all(i + j == k for i, j, k in nwise(islice(fibonacci(), n), 3))


def test_three_values():
    assert list(islice(fibonacci(), 3)) == [1, 1, 2]
