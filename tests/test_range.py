from pytest import raises

from range import Range


def test_simple_range():
    r = Range(10)
    assert len(r) == 10
    assert list(r) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert list(reversed(r)) == [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    assert 0 in r
    assert 1 in r
    assert 9 in r
    assert 10 not in r
    assert -1 not in r


def test_range_not_equal_list():
    r = Range(3)
    assert r != [0, 1, 2]


def test_step_should_be_non_zero():
    with raises(ValueError):
        r = Range(1, 10, 0)
    with raises(ValueError):
        r = Range(1.0, 10.0, 0.0)


def test_different_params():
    assert list(Range(1, 3)) == [1, 2]
    assert list(Range(1, 3, 2)) == [1]
    assert list(Range(1, 11, 2)) == [1, 3, 5, 7, 9]
    assert list(Range(1, 12, 2)) == [1, 3, 5, 7, 9, 11]
    assert list(Range(1, 10, -1)) == []
    assert list(Range(10, 8, 2)) == []
    assert list(Range(-10)) == []
    assert list(Range(-7, -2)) == [-7, -6, -5, -4, -3]
    assert list(Range(-3, -7, -2)) == [-3, -5]
    assert list(Range(5, 15, -2)) == []
    assert list(Range(13., 16., 0.5)) == [13., 13.5, 14., 14.5, 15., 15.5]
    assert list(Range(13, 16, 0.5)) == [13, 13.5, 14, 14.5, 15, 15.5]


def test_range_equal():
    range0 = Range(0, 10, 5)
    range1 = Range(0, 11, 5)
    range2 = Range(0, 12, 5)
    range3 = Range(0, 15, 5)
    assert range0 != range1
    assert hash(range1) == hash(range2)
    assert range1 == range2
    assert hash(range1) == hash(range3)
    assert range1 == range3
    assert hash(range2) == hash(range3)
    assert range2 == range3


def test_range_contains():
    range = Range(0.0, 100.0, 5.)
    assert 0.0 in range
    assert 5.0 in range
    assert 10.0 in range
    assert 95.0 in range
    assert 100.0 not in range
    assert 105.0 not in range
    assert 3.0 not in range


def test_range_getitem():
    r = Range(0.0, 100.0, 5.)
    assert r[0] == 0.0
    assert r[1] == 5.0
    assert r[19] == 95.0
    with raises(IndexError):
        x = r[-1]
    with raises(IndexError):
        x = r[20]
    with raises(IndexError):
        x = r[21]
