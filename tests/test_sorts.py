import random

from sorts import is_sorted, merge_sort, quicksort, radix_sort, wrapper


def test_merge_sort():
    n = 1000
    lst = [i for i in range(n)]
    random.shuffle(lst)
    assert is_sorted(merge_sort(lst))


def test_quicksort():
    n = 1000
    lst = [i for i in range(n)]
    random.shuffle(lst)
    assert is_sorted(quicksort(lst))


def test_radix_sort():
    n = 1000
    lst = [i for i in range(n)]
    random.shuffle(lst)
    assert is_sorted(radix_sort(lst))


def test_wrapper():
    s = '123 17 12 24 13 144'
    assert wrapper(s, quicksort) == [12, 13, 17, 24, 123, 144]
