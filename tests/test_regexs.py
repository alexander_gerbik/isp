from pytest import raises

from regexs import is_float, parse_uri, is_email


def test_email():
    assert is_email('alex@gmail.com')
    assert not is_email('asfd.gmail.com')


def test_float():
    assert is_float('1.0')
    assert is_float('-5.')
    assert is_float('+5.')
    assert is_float('3.2e12')
    assert is_float('3.2E12')
    assert is_float('3.2E+12')
    assert is_float('3.2E-12')
    assert is_float('5.')
    assert is_float('.2')
    assert is_float('0.0')
    assert not is_float('5')
    assert not is_float('.')
    assert not is_float('--5.')
    assert not is_float('+-5.')
    assert not is_float('++5.')
    assert not is_float('3.2E++12')
    assert not is_float('3.2E--12')
    assert not is_float('3.2E+-12')
    assert not is_float('3.2E-+12')


def test_uri():
    s = ('ftp://user:pass@www.cs.server.com:8080'
         '/dir1/dir2/file.php?param1=value1#hashtag')
    res = parse_uri(s)
    assert res['scheme'] == 'ftp'
    assert res['username'] == 'user'
    assert res['password'] == 'pass'
    assert res['host'] == 'www.cs.server.com'
    assert res['port'] == '8080'
    assert res['path'] == '/dir1/dir2/file.php'
    assert res['query'] == 'param1=value1'
    assert res['fragment'] == 'hashtag'


def test_uri_invalid():
    with raises(ValueError):
        parse_uri('qwe')
