from cache import cached


def test_cached():
    times_called = 0

    @cached
    def some_func(a, b, c=None, **kwargs):
        nonlocal times_called
        times_called += 1
        doubled = 0 if 'doubled' not in kwargs else kwargs['doubled']
        subtracted = 0 if 'subtracted' not in kwargs else kwargs['subtracted']
        result = 0 if c is None else c
        return result + a + b - subtracted + 2 * doubled

    assert some_func(2, 3) == 5
    assert times_called == 1
    assert some_func(2, 3) == 5
    assert times_called == 1

    assert some_func(2, 3, 5) == 10
    assert times_called == 2
    assert some_func(2, 3, 5) == 10
    assert times_called == 2

    assert some_func(2, 3, 5, doubled=7) == 24
    assert times_called == 3
    assert some_func(2, 3, 5, doubled=7) == 24
    assert times_called == 3

    assert some_func(2, 3, 5, subtracted=7) == 3
    assert times_called == 4
    assert some_func(2, 3, 5, subtracted=7) == 3
    assert times_called == 4

    assert some_func(2, 3, 5, subtracted=7, doubled=6) == 15
    assert times_called == 5
    assert some_func(2, 3, 5, subtracted=7, doubled=6) == 15
    assert times_called == 5
