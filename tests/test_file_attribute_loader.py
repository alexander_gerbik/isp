import tempfile

from pytest import raises

from file_attribute_loader import FileAttributeLoader


def test_loader():
    file = tempfile.TemporaryFile('w+t')
    file.write("""
    qwe=asd
    zxc=zc
    """)
    file.seek(0)

    class A(metaclass=FileAttributeLoader, file=file):
        pass

    assert A.qwe == 'asd'
    assert A.zxc == 'zc'


def test_loader_no_param():
    with raises(RuntimeError):
        class B(metaclass=FileAttributeLoader):
            pass


def test_loader_not_valid_identifier():
    file = tempfile.TemporaryFile('w+t')
    file.write("""
    2qwe=asd
    zxc=zc
    """)
    file.seek(0)
    with raises(SyntaxError):
        class C(metaclass=FileAttributeLoader, file=file):
            pass
