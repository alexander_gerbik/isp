from pytest import raises

from vector import Vector


def test_raises_if_not_iterable():
    with raises(TypeError):
        Vector(5)


def test_raises_if_empty():
    with raises(ValueError):
        Vector([])


def test_raises_if_not_number():
    with raises(TypeError):
        Vector([0, 1, 'qwe', 3])


def test_zeroes():
    a = Vector.zeroes(5)
    b = Vector([0.0, 0.0, 0.0, 0.0, 0.0])
    assert a == b


def test_len():
    b = Vector([0.0, 0.0, 0.0, 0.0, 0.0])
    assert len(b) == 5


def test_iter():
    b = Vector([2, 2, 3, 4])
    assert [2, 2, 3, 4] == list(iter(b))


def test_add_sub_mul_should_have_equal_length():
    a = Vector([2, 2, 3])
    b = Vector([2, 2, 3, 4])
    with raises(ValueError):
        a + b
    with raises(ValueError):
        a - b
    with raises(ValueError):
        a * b
    with raises(ValueError):
        a.dot(b)


def test_add():
    a = Vector([1, 2, 3, 4])
    b = Vector([2, 7, 14, 8])
    assert Vector([3, 9, 17, 12]) == a + b
    assert b + a == a + b


def test_sub():
    a = Vector([1, 2, 3, 4])
    b = Vector([2, 7, 14, 8])
    assert Vector([1, 5, 11, 4]) == b - a


def test_equal():
    c = Vector(range(4))
    a = Vector([0, 1, 2, 3])
    assert c == a


def test_raises_if_type_incorrect():
    a = Vector(range(4))
    b = list(range(4))
    assert a != b
    with raises(TypeError):
        a + b
    with raises(TypeError):
        a - b


def test_get_item():
    a = Vector([12, 24, 72, 198])
    assert a[0] == 12
    assert a[1] == 24
    assert a[2] == 72
    assert a[3] == 198
    with raises(IndexError):
        a[4]


def test__repr_str():
    a = Vector(range(3))
    assert str(a) == '[0, 1, 2]'
    assert repr(a) == 'Vector([0, 1, 2])'


def test_not_equal():
    a = Vector([0, 1, 2, 3])
    b = Vector([0, 1, 2, 4])
    c = Vector([0, 1, 2])
    assert a != b
    assert a != c


def test_mul_by_vector():
    a = Vector([0, 144, 7, 11])
    b = Vector([5, 000, 3, 13])
    assert a * b == 164
    assert a * b == b * a


def test_mul_by_number():
    a = Vector([0, 1, 2, 3])
    b = 7
    assert a * b == Vector([0, 7, 14, 21])
    assert a * b == b * a


def test_mul_by_nonsense():
    a = Vector([0, 1, 2, 3])
    b = 'qwe'
    with raises(TypeError):
        a * b
