from pytest import raises

from custom_filter import Filter


def is_even(number):
    return number % 2 == 0


def test_filter():
    items = range(10)
    filter = Filter(is_even, items)
    assert list(filter) == [0, 2, 4, 6, 8]


def test_invalid_arguments():
    with raises(TypeError):
        filter = Filter('qwe', [])
    with raises(TypeError):
        filter = Filter(is_even, print)
