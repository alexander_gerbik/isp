from singleton import Singleton


def test_singleton():
    class A(metaclass=Singleton):
        pass

    class B(A):
        pass
    a = A()
    a1 = A()
    b = B()
    assert a is a1
    assert b is not a
    assert B.instance is not A.instance
