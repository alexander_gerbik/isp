from multiprocessing import Lock
from pytest import raises

from my_json import to_json, UnformattableTypeError


def test_to_json():
    o = ['foo', {'bar': ('b"az', None, 1.0, 2)}]
    assert to_json(o) == '["foo", {"bar": ["b\\\"az", null, 1.0, 2]}]'


def test_to_json_num_keys():
    o = {'5': True, 6: False, 5.0: 7}
    assert to_json(o) == '{"5": true, "6": false, "5.0": 7}'


def test_wrong_type():
    with raises(UnformattableTypeError) as e:
        to_json(Lock())
    assert 'multiprocessing.synchronize.Lock' in str(e)
    del e
