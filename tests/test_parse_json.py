from pytest import raises

from my_json import parse_json
from my_tokenize import Token, tokenize, TokenIterator


class Whatever(object):
    def __eq__(self, other):
        return True


whatever = Whatever()


def test_tokenization():
    tokens = list(tokenize('["foo", {"bar": ["b\\\"az", null, 1.0, 2]}]'))
    expected = [
        Token('OPEN_BRACKETS', whatever),
        Token('STRING', 'foo'),
        Token('ITEM_SEPARATOR', whatever),
        Token('OPEN_BRACES', whatever),
        Token('STRING', 'bar'),
        Token('KEY_VALUE_SEPARATOR', whatever),
        Token('OPEN_BRACKETS', whatever),
        Token('STRING', 'b"az'),
        Token('ITEM_SEPARATOR', whatever),
        Token('NULL', None),
        Token('ITEM_SEPARATOR', whatever),
        Token('FLOAT', 1.0),
        Token('ITEM_SEPARATOR', whatever),
        Token('INT', 2),
        Token('CLOSING_BRACKETS', whatever),
        Token('CLOSING_BRACES', whatever),
        Token('CLOSING_BRACKETS', whatever),
    ]
    assert tokens == expected


def test_unexpected_token():
    with raises(RuntimeError) as exc:
        tokens = list(tokenize('["foo", {"bar": ["b\\\"az", null, qwe, 2]}]'))
    assert str(exc.value) == "Unexpected token: 'qwe, 2]}]'"
    del exc


def test_iterator():
    source = '["foo", {"bar": ["b\\\"az", null, 1.0, 2]}]'
    iterator = TokenIterator(tokenize(source))
    iterator.assert_next('OPEN_BRACKETS')
    assert next(iterator) == Token('STRING', 'foo')
    with raises(SyntaxError) as exc:
        iterator.assert_next('KEY_VALUE_SEPARATOR')
    assert ('Expected token of type KEY_VALUE_SEPARATOR,'
            ' got token of type ITEM_SEPARATOR instead' == exc.value.msg)
    del exc
    assert iterator.next == Token('OPEN_BRACES', whatever)
    assert iterator.next == Token('OPEN_BRACES', whatever)
    assert list(iterator) == [
        Token('OPEN_BRACES', whatever),
        Token('STRING', 'bar'),
        Token('KEY_VALUE_SEPARATOR', whatever),
        Token('OPEN_BRACKETS', whatever),
        Token('STRING', 'b"az'),
        Token('ITEM_SEPARATOR', whatever),
        Token('NULL', None),
        Token('ITEM_SEPARATOR', whatever),
        Token('FLOAT', 1.0),
        Token('ITEM_SEPARATOR', whatever),
        Token('INT', 2),
        Token('CLOSING_BRACKETS', whatever),
        Token('CLOSING_BRACES', whatever),
        Token('CLOSING_BRACKETS', whatever),
    ]


def test_parse():
    source = '["foo", {"bar": ["b\\\"az", null, 1.0, 2]}]'
    assert parse_json(source) == ['foo', {'bar': ['b"az', None, 1.0, 2]}]


def test_parse_num_keys():
    source = '{"5": true, "6": false, "5.0": 7}'
    assert parse_json(source) == {'5': True, '6': False, '5.0': 7}


def test_parse_left_tokens():
    source = '["foo", {"bar": ["b\\\"az", null, 1.0, 2]}] }, null'
    with raises(SyntaxError):
        parse_json(source)


def test_parse_empty():
    source = '{"5": [], "6": {}, "5.0": 7}'
    assert parse_json(source) == {'5': [], '6': {}, '5.0': 7}


def test_parse_unexpected_token():
    source = '{"5": }, "6": {}, "5.0": 7}'
    with raises(SyntaxError):
        parse_json(source)
