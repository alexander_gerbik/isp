from pytest import raises

from fields import StringField, IntField, PositiveIntField, Field, Model, \
    PositiveFloatField


class Person(metaclass=Model):
    age = PositiveIntField()
    name = StringField()
    money = IntField()


class Student(Person):
    average_mark = PositiveFloatField()


def test_descriptor_returns_self():
    descriptor = Person.age
    assert isinstance(descriptor, PositiveIntField)


def test_field():
    class A(object):
        f = Field()

    a = A()
    a.f = 'qwe'
    assert a.f == 'qwe'
    del a.f
    with raises(AttributeError):
        print('qwe' == a.f)


def test_str_field():
    class A(object):
        s = StringField()

    a = A()
    a.s = 'qwe'
    assert a.s == 'qwe'
    with raises(TypeError):
        a.s = 123


def test_int_field():
    class B(object):
        i = IntField()

    b = B()
    b.i = 123
    assert b.i == 123
    with raises(TypeError):
        b.i = 123.0


def test_positive_int_field():
    class C(object):
        i = PositiveIntField()

    c = C()
    c.i = 123
    assert c.i == 123
    c.i = 0
    assert c.i == 0
    with raises(ValueError):
        c.i = -1
    with raises(TypeError) as info:
        c.i = 'qwe'
    assert info.match("value is not an instance of <class 'int'>")
    del info


def test_model():
    p = Person(12, 'Alex', -5)
    assert p.age == 12
    assert p.name == 'Alex'
    assert p.money == -5
    p = Person(18, name='Ben', money=12)
    assert p.age == 18
    assert p.name == 'Ben'
    assert p.money == 12
    p = Person(age=24, name='Zoey', money=37)
    assert p.age == 24
    assert p.name == 'Zoey'
    assert p.money == 37
    with raises(ValueError):
        p = Person(age=-27, name='Zoey', money=32)
    with raises(TypeError):
        p = Person(age=27, name=123, money=32)
    with raises(TypeError):
        p = Person(age=27, name='Alex')
    with raises(TypeError):
        p = Person(27, 'Alex', age=32)


def test_inherited_model():
    s = Student(27, name='Alex', money=-5, average_mark=3.5)
    assert s.age == 27
    assert s.name == 'Alex'
    assert s.money == -5
    assert s.average_mark == 3.5
    with raises(TypeError):
        s = Student(27, name=13, money=-5, average_mark=3.5)


def test_extra_kwargs():
    with raises(TypeError):
        p = Person(age=5, name='Alex', money=32, salary=522)
