from expanding_dict import ExpandingDict


def test_set_get_item():
    d = ExpandingDict()
    d['a'] = 1
    assert 'a' in d
    assert d['a'] == 1
    assert str(d) == "{'a': 1}"


def test_get_nonexistent_item():
    d = ExpandingDict()
    assert 'qwe' not in d
    item = d['qwe']
    assert isinstance(item, ExpandingDict)


def test_repr_and_str():
    d = ExpandingDict()
    d['a']['b'] = 1
    d['q']['w']['e'] = 123
    d['q']['w']['d'] = '74'
    d['z']['a'] = {'d': 327}
    assert str(d) == ("{'a': {'b': 1}, 'q': {'w': {'e': 123, 'd': '74'}}, "
                      "'z': {'a': {'d': 327}}}")
    assert repr(d) == ("ExpandingDict(**{'a': ExpandingDict(**{'b': 1}), "
                       "'q': ExpandingDict(**{'w': ExpandingDict(**{'e': 123, "
                       "'d': '74'})}), "
                       "'z': ExpandingDict(**{'a': {'d': 327}})})")
    assert d == eval(repr(d))
