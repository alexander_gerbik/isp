from logged import Logged, Logger


def test_logged():
    class A(Logged):
        def func(self):
            return 2

        def func2(self, a, b):
            return a + b

        def func3(self, *args):
            return sum(args)

        def func4(self, *args, **kwargs):
            return sum(args) - sum(len(k) for k in kwargs)

    a = A()
    x = a.func()
    y = a.func2(5, 12)
    z = a.func3(5, 4, 2, 1, 3)
    w = a.func4(5, 4, 2, 1, 3, key=12, another=13)
    expected = """
    func(*(self,), **{}) -> 2
    func2(*(self, 5, 12), **{}) -> 17
    func3(*(self, 5, 4, 2, 1, 3), **{}) -> 15
    func4(*(self, 5, 4, 2, 1, 3), **{'key': 12, 'another': 13}) -> 5
    """.strip().replace('    ', '')
    assert str(a) == expected


def test_logged_formated():
    class B(Logged):
        __logger__ = Logger('{return}: {name}! {args} # {kwargs}')

        def func(self):
            return 2

        def func2(self, a, b):
            return a + b

        def func3(self, *args):
            return sum(args)

        def func4(self, *args, **kwargs):
            return sum(args) - sum(len(k) for k in kwargs)

    b = B()
    x = b.func()
    y = b.func2(5, 12)
    z = b.func3(5, 4, 2, 1, 3)
    w = b.func4(5, 4, 2, 1, 3, key=12, another=13)
    expected = """
        2: func! (self,) # {}
        17: func2! (self, 5, 12) # {}
        15: func3! (self, 5, 4, 2, 1, 3) # {}
        5: func4! (self, 5, 4, 2, 1, 3) # {'key': 12, 'another': 13}
        """.strip().replace('    ', '')
    assert str(b) == expected
