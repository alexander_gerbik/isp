import random

from statistics import (iter_sentences, words_per_sentence, average,
                        order_statistic, most_common_ngram, count_words)

text = """
The path I choose through the maze makes me what I am.
I am not only a thing, but also a way of being--one of many ways--and knowing
the paths I have followed and the ones left to take will help me understand
what I am becoming.
Strange about learning; the farther I go the more I see that I never knew
even existed.
A short while ago I foolishly thought I could learn everything
- all the knowledge in the world.
Now I hope only to be able to know of
its existence, and to understand one grain of it... Is there time?"""


def test_iter_sentences():
    t = 'One sentence. Second sentence!!! Third sentence?! Fourth sentence?'
    result = list(iter_sentences(t))
    expected = ['One sentence', ' Second sentence', ' Third sentence',
                ' Fourth sentence']
    assert result == expected


def test_word_count():
    t = 'One sentence. Second sentence!!! Third sentence?! Fourth sentence?'
    assert count_words(t) == {'sentence': 4, 'One': 1, 'Second': 1, 'Third': 1,
                              'Fourth': 1}


def test_word_per_sentence():
    assert list(words_per_sentence(text)) == [12, 37, 17, 17, 19, 3]


def test_average():
    assert average([1, 12, 17, 23]) == 53 / 4


def test_order_statistic():
    n = 20
    lst = [2 ** i for i in range(n)]
    random.shuffle(lst)
    for i in range(n):
        assert order_statistic(lst, i) == 2 ** i


def test_median_odd():
    lst = [13, 2, 7, 14, 25]
    assert order_statistic(lst) == 13


def test_median_even():
    lst = [13, 2, 14, 25]
    assert order_statistic(lst) == 14


def test_most_common_ngram():
    assert most_common_ngram(text, n=3, k=3) == [
        (('t', 'h', 'e'), 9),
        (('i', 'n', 'g'), 6),
        (('a', 'n', 'd'), 5),
    ]
