import tempfile
from io import StringIO
from contextlib import redirect_stdout, _RedirectStream

from pytest import raises

from storage import main


class RedirectStdin(_RedirectStream):
    """Context manager for temporarily redirecting another file to stdin."""

    _stream = "stdin"


def execute(command):
    input = StringIO(command)
    output = StringIO()
    with redirect_stdout(output), RedirectStdin(input):
        main()
    return output.getvalue()


def str_to_set(s):
    return set(s.strip().split('\n'))


def test_add_item():
    command = ('add qwe asd zxc qwe\n'
               'list\n'
               '\n')
    expected = 'zxc\nasd\nqwe\n'
    result = execute(command)
    assert str_to_set(result) == str_to_set(expected)


def test_remove_item():
    command = ('add qwe asd zxc\n'
               'remove qwe\n'
               'remove bnm\n'
               'list\n'
               '\n')
    expected = 'zxc\nasd\n'
    result = execute(command)
    assert str_to_set(result) == str_to_set(expected)


def test_find_item():
    command = ('add qwe asd zxc\n'
               'find qwe yui zxc\n'
               '\n')
    expected = 'qwe\nzxc\n'
    result = execute(command)
    assert str_to_set(result) == str_to_set(expected)


def test_grep():
    command = ('add qwe qwwwwwwe qwasdeee qe qwwe qsse\n'
               'grep qw+e\n'
               '\n')
    expected = 'qwe\nqwwwwwwe\nqwwe\n'
    result = execute(command)
    assert str_to_set(result) == str_to_set(expected)


def test_save_load():
    with tempfile.NamedTemporaryFile() as file:
        command = ('add qwe asd zxc\n'
                   'save {filename}\n'
                   'remove qwe asd zxc\n'
                   'add ghj\n'
                   'list\n'
                   'load {filename}\n'
                   'list\n'
                   '\n').format(filename=file.name)
        expected = 'ghj\nqwe\nasd\nzxc\n'
        result = execute(command)
    assert str_to_set(result) == str_to_set(expected)


def test_invalid_command():
    command = ('unexpected qwe\n'
               '\n')
    with raises(ValueError) as exc:
        execute(command)

    assert str(exc.value) == 'Invalid command: unexpected'
    del exc


def test_save_multiple_args():
    command = ('save /tmp/file1 /tmp/file2\n'
               '\n')
    with raises(ValueError) as exc:
        execute(command)

    assert str(exc.value) == 'Command expects one argument'
    del exc


def test_load_multiple_args():
    command = ('load /tmp/file1 /tmp/file2\n'
               '\n')
    with raises(ValueError) as exc:
        execute(command)

    assert str(exc.value) == 'Command expects one argument'
    del exc
