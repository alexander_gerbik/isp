from collections import Counter
from re import finditer

from utils import nwise


def iter_sentences(text):
    for match in finditer(r'[^.?!]+', text):
        yield match.group(0)


def iter_words(text):
    for match in finditer(r'\w+', text):
        yield match.group(0)


def count_words(text):
    return Counter(iter_words(text))


def words_per_sentence(text):
    for sentence in iter_sentences(text):
        yield sum(1 for _ in iter_words(sentence))


def average(iterable):
    s = 0
    a = 0
    for item in iterable:
        s += item
        a += 1
    return s / a


def _partition(array, begin, end):
    pivot = begin
    for i in range(begin + 1, end + 1):
        if array[i] <= array[begin]:
            pivot += 1
            array[i], array[pivot] = array[pivot], array[i]
    array[pivot], array[begin] = array[begin], array[pivot]
    return pivot


def order_statistic(lst, k=None):
    lst = lst.copy()
    if k is None:
        k = len(lst) // 2
    li, ri = 0, len(lst) - 1
    assert li <= k <= ri
    while True:
        p = _partition(lst, li, ri)
        if p == k:
            return lst[p]
        if p < k:
            li = p + 1
        else:
            ri = p - 1


def most_common_ngram(text, n, k):
    counter = Counter(
        ngram for word in iter_words(text) for ngram in nwise(word, n))
    return counter.most_common(k)
