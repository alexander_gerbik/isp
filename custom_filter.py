from collections.abc import Iterable


class Filter(object):
    def __init__(self, predicate, iterable):
        if not isinstance(iterable, Iterable):
            raise TypeError('iterable must support iteration')
        if not callable(predicate):
            raise TypeError('predicate must be callable')
        self._items = list(iterable)
        self._predicate = predicate

    def __iter__(self):
        for item in self._items:
            if self._predicate(item):
                yield item
