from functools import wraps


class Logger(object):
    def __init__(self, format='{name}(*{args}, **{kwargs}) -> {return}'):
        self._records = []
        self._format = format

    def _register(self, function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            self._records.append((function.__name__, args, kwargs, result))
            return result

        return wrapper

    def _emit(self):
        def kw(r):
            return {
                'name': r[0],
                'args': r[1],
                'kwargs': r[2],
                'return': r[3],
            }

        result = '\n'.join(self._format.format(**kw(r)) for r in self._records)
        self._records.clear()
        return result


class _LoggedMeta(type):
    def __new__(mcs, name, bases, namespace):
        logger = namespace.setdefault('__logger__', Logger())
        for k in namespace:
            if callable(namespace[k]):
                namespace[k] = logger._register(namespace[k])
        namespace['__str__'] = logger._emit
        namespace['__repr__'] = lambda self: 'self'
        return super().__new__(mcs, name, bases, namespace)


class Logged(object, metaclass=_LoggedMeta):
    __logger__ = Logger()
