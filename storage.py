import pickle
import re


class Storage(object):
    _commands = {}

    def __init__(self):
        self._state = set()

    @classmethod
    def register(cls, function):
        cls._commands[function.__name__] = function
        return function

    def execute(self, command, args):
        if command not in self._commands:
            raise ValueError('Invalid command: {}'.format(command))
        return self._commands[command](self, args)


@Storage.register
def add(self, args):
    for i in args:
        self._state.add(i)


@Storage.register
def remove(self, args):
    for i in args:
        self._state.discard(i)


@Storage.register
def find(self, args):
    for i in args:
        if i in self._state:
            print(i)


@Storage.register
def list(self, args):
    for i in self._state:
        print(i)


@Storage.register
def grep(self, args):
    if len(args) != 1:
        raise ValueError('Command expects one argument')
    pattern = args[0]
    for i in self._state:
        if re.fullmatch(pattern, i):
            print(i)


@Storage.register
def save(self, args):
    if len(args) != 1:
        raise ValueError('Command expects one argument')
    filename = args[0]
    with open(filename, 'wb') as file:
        pickle.dump(self._state, file)


@Storage.register
def load(self, args):
    if len(args) != 1:
        raise ValueError('Command expects one argument')
    filename = args[0]
    with open(filename, 'rb') as file:
        self._state = pickle.load(file)


def main():
    storage = Storage()
    for entered in iter(lambda: input().strip(), ''):
        command, *args = entered.split()
        storage.execute(command, args)


if __name__ == '__main__':
    main()
