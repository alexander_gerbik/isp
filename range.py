class Range(object):
    def __init__(self, start, stop=None, step=None):
        start, stop, step = self._normalize(start, stop, step)
        self.start = start
        self.stop = stop
        self.step = step

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, value):
        if not value:
            raise ValueError('step should be not zero')
        self._step = value

    @property
    def _points_amount(self):
        amount, residue = divmod(self.stop - self.start, self.step)
        if residue:
            amount += 1
        amount = max(0, amount)
        return amount

    def __reversed__(self):
        start = self.start + self.step * (self._points_amount - 1)
        stop = self.start - self.step
        step = -self.step
        return Range(start, stop, step)

    def __len__(self):
        return self._points_amount

    def __iter__(self):
        for i in self._iter_points():
            yield self.start + i * self.step

    def __hash__(self):
        return hash((self.start, self.step, self._points_amount))

    def __eq__(self, other):
        if not isinstance(other, Range):
            return NotImplemented
        return ((self.start, self.step, self._points_amount) ==
                (other.start, other.step, other._points_amount))

    def __getitem__(self, item):
        if not (0 <= item < self._points_amount):
            raise IndexError("Given value is beyond range's boundaries")
        return self.start + self.step * item

    def __contains__(self, item):
        order, residue = divmod(item - self.start, self.step)
        return not residue and 0 <= order < self._points_amount

    def _iter_points(self):
        i = 0
        while i < self._points_amount:
            yield i
            i += 1

    @staticmethod
    def _normalize(start, stop=None, step=None):
        if step is None:
            step = 1
            if stop is None:
                start, stop = 0, start
        return start, stop, step
